﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DocumentImageLister
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            DocumentOrientationBox.SelectedIndex = 0;
            DocumentSizeBox.SelectedItem = "A4";
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            var result = folderBrowser.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK || result == System.Windows.Forms.DialogResult.Yes)
                pathTextBox.Text = folderBrowser.SelectedPath;
        }

        private void generatePdfBtn_Click(object sender, EventArgs e)
        {
            int dpi = (int)pdfDpiNum.Value;
            int compressionLevel = (int)compression.Value;

            PdfDocumentWriter pdf = new PdfDocumentWriter()
            {
                DPI = dpi,
                CompressionLevel = compressionLevel,

                DocMargins = new Rect(
                    Constants.ToDots((float)docMarginLeftNum.Value, dpi), 
                    Constants.ToDots((float)docMarginUpNum.Value, dpi),
                    Constants.ToDots((float)docMarginRightNum.Value, dpi),
                    Constants.ToDots((float)docMarginDownNum.Value, dpi)),

                PicMargins = new Rect(
                    Constants.ToDots((float)picMarginLeftNum.Value, dpi), 
                    Constants.ToDots((float)picMarginUpNum.Value, dpi), 
                    Constants.ToDots((float)picMarginRightNum.Value, dpi),
                    Constants.ToDots((float)picMarginDownNum.Value, dpi)),
            };

            pdf.SetPageSize(DocumentSizeBox.SelectedItem.ToString(), DocumentOrientationBox.SelectedIndex == 1);

            Generate(pdf);
        }

        private void generateHtmlButton_Click(object sender, EventArgs e)
        {
            int dpi = (int)BrowserDpiNum.Value;

            HtmlDoucumentWriter html = new HtmlDoucumentWriter()
            {
                DPI = dpi,
                PicMargins = new Rect(
                    Constants.ToDots((float)picMarginLeftNum.Value, dpi), 
                    Constants.ToDots((float)picMarginUpNum.Value, dpi), 
                    Constants.ToDots((float)picMarginRightNum.Value, dpi),
                    Constants.ToDots((float)picMarginDownNum.Value, dpi)),
            };

            Generate(html);

        }

        private void Generate(DocumentWriter doc)
        {
            string path = pathTextBox.Text;
            if (!Directory.Exists(path))
            {
                Log("Error: Path does not exist!");
                return;
            }


            doc.CreateDocument(path);


            string[] supportedFormats = new string[] { "*.png", "*.jpg", "*.jpeg", "*.bmp" };

            foreach (string searchPattern in supportedFormats)
            {
                foreach (var f in Directory.EnumerateFiles(path, searchPattern, SearchOption.TopDirectoryOnly))
                {
                    System.Drawing.Image img = System.Drawing.Image.FromFile(f);



                    for (int i = 0; i < countNum.Value; i++)
                    {
                        doc.AddImage(f, img.Width, img.Height, (int)dpiNum.Value);
                    }

                    img.Dispose();
                }
            }

            doc.CloseDocument();

          
            //Log(string.Format("HTML File created in {0}", file));

        }

        void Log(string msg)
        {
            output.Text = msg;
        }


    }
}
