﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentImageLister
{
    public static class Constants
    {
        const float InchesPerMillimeter = 1f / 25.4f;

        public static float ToDots(float millimeters, int dpi)
        {
            return InchesPerMillimeter * millimeters * dpi;
        }
    }
}
