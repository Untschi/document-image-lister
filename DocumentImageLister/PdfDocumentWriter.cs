﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DocumentImageLister
{

    public class PdfDocumentWriter : DocumentWriter
    {

        public int DPI;
        public int CompressionLevel = 8;
        protected override int DocumentDPI { get { return DPI; } }

        List<iTextSharp.text.Image> queue = new List<Image>();
        float curWidth, curHeight, lastY;

        public Rect DocMargins;
        public Rect PicMargins;

        float PageWidth { get { return doc.PageSize.Width - doc.LeftMargin - doc.RightMargin; } }
        float PageHeight { get { return doc.PageSize.Height - doc.TopMargin - doc.BottomMargin; } }

        string path;
        string filePath;
        Document doc;

        Rectangle pageSize;

        public override void CreateDocument(string path)
        {
            this.path = path;
            this.filePath = Path.Combine(path, "images.pdf");

            doc = new Document(pageSize);
            doc.SetMargins(DocMargins.Left, DocMargins.Width, DocMargins.Top, DocMargins.Height);

            var writer = PdfWriter.GetInstance(doc, new FileStream(filePath, FileMode.Create));
            writer.SetPdfVersion(PdfWriter.PDF_VERSION_1_5);
            writer.CompressionLevel = CompressionLevel;


            doc.Open();


            curWidth = 0;
            curHeight = 0;
            lastY = doc.Top - doc.TopMargin;
        }

        public override void CloseDocument()
        {
            BlitImages();
            doc.CloseDocument();


            System.Diagnostics.Process.Start(filePath);
        }

        protected override void AddImage(string imagePath, int width, int height)
        {
            iTextSharp.text.Image pic = iTextSharp.text.Image.GetInstance(imagePath, true);
            pic.CompressionLevel = CompressionLevel;
            

            //pic.SetDpi(300, 300);
            pic.ScaleAbsolute(width, height);

            float w = width + PicMargins.Right;

            if(curWidth + w > PageWidth)
            {
                BlitImages();
            }

            queue.Add(pic);

            curWidth += w;
            curHeight = Math.Max(curHeight, height + PicMargins.Bottom);

            //pic.Alignment = 1;
            pic.SimplifyColorspace();

        }

        private void BlitImages()
        {
            if(lastY - curHeight < DocMargins.Bottom)
            {
                doc.NewPage();
                lastY = doc.Top - doc.TopMargin;
            }

            float left = 0;
            foreach(var pic in queue)
            {
                float x = DocMargins.Left + (left + PicMargins.Left) + 0.5f * (PageWidth - curWidth);
                float y = lastY - PicMargins.Top - curHeight;

                pic.SetAbsolutePosition(x, y);
                left += pic.ScaledWidth + PicMargins.Right;
                doc.Add(pic);
            }

            queue.Clear();

            lastY -= curHeight;
            curWidth = 0;
            curHeight = 0;
        }


        internal void SetPageSize(string documentSize, bool isLandscape)
        {
            FieldInfo field = typeof(PageSize).GetField(documentSize, BindingFlags.Static | BindingFlags.Public);
            pageSize = field.GetValue(null) as Rectangle;

            if (isLandscape)
                pageSize = pageSize.Rotate();
        }
    }
}
