﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentImageLister
{
    public struct Rect
    {
        public float Left, Top, Width, Height;

        public float Right { get { return Left + Width; } }
        public float Bottom { get { return Top + Height; } }

        public Rect(float left, float top, float width, float height)
        {
            this.Left = left;
            this.Top = top;
            this.Width = width;
            this.Height = height;
        }
    }
}
