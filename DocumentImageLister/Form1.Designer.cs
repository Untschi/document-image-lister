﻿namespace DocumentImageLister
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GenerateHtmlButton = new System.Windows.Forms.Button();
            this.folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.browseButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pathTextBox = new System.Windows.Forms.TextBox();
            this.output = new System.Windows.Forms.Label();
            this.BrowserDpiNum = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.dpiNum = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.countNum = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPdf = new System.Windows.Forms.TabPage();
            this.label8 = new System.Windows.Forms.Label();
            this.compression = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.DocumentOrientationBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.DocumentSizeBox = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.docMarginRightNum = new System.Windows.Forms.NumericUpDown();
            this.docMarginLeftNum = new System.Windows.Forms.NumericUpDown();
            this.docMarginDownNum = new System.Windows.Forms.NumericUpDown();
            this.docMarginUpNum = new System.Windows.Forms.NumericUpDown();
            this.generatePdfBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.pdfDpiNum = new System.Windows.Forms.NumericUpDown();
            this.tabHtml = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.picMarginRightNum = new System.Windows.Forms.NumericUpDown();
            this.picMarginLeftNum = new System.Windows.Forms.NumericUpDown();
            this.picMarginDownNum = new System.Windows.Forms.NumericUpDown();
            this.picMarginUpNum = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.BrowserDpiNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dpiNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.countNum)).BeginInit();
            this.tabControl.SuspendLayout();
            this.tabPdf.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.compression)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.docMarginRightNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.docMarginLeftNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.docMarginDownNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.docMarginUpNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdfDpiNum)).BeginInit();
            this.tabHtml.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMarginRightNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMarginLeftNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMarginDownNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMarginUpNum)).BeginInit();
            this.SuspendLayout();
            // 
            // GenerateHtmlButton
            // 
            this.GenerateHtmlButton.Location = new System.Drawing.Point(6, 122);
            this.GenerateHtmlButton.Name = "GenerateHtmlButton";
            this.GenerateHtmlButton.Size = new System.Drawing.Size(474, 44);
            this.GenerateHtmlButton.TabIndex = 0;
            this.GenerateHtmlButton.Text = "Generate HTML File";
            this.GenerateHtmlButton.UseVisualStyleBackColor = true;
            this.GenerateHtmlButton.Click += new System.EventHandler(this.generateHtmlButton_Click);
            // 
            // browseButton
            // 
            this.browseButton.Location = new System.Drawing.Point(694, 13);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(30, 23);
            this.browseButton.TabIndex = 1;
            this.browseButton.Text = "...";
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Image Folder:";
            // 
            // pathTextBox
            // 
            this.pathTextBox.Location = new System.Drawing.Point(90, 14);
            this.pathTextBox.Name = "pathTextBox";
            this.pathTextBox.Size = new System.Drawing.Size(598, 20);
            this.pathTextBox.TabIndex = 3;
            // 
            // output
            // 
            this.output.AutoSize = true;
            this.output.Location = new System.Drawing.Point(9, 239);
            this.output.Name = "output";
            this.output.Size = new System.Drawing.Size(16, 13);
            this.output.TabIndex = 4;
            this.output.Text = "...";
            // 
            // BrowserDpiNum
            // 
            this.BrowserDpiNum.Location = new System.Drawing.Point(91, 6);
            this.BrowserDpiNum.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.BrowserDpiNum.Name = "BrowserDpiNum";
            this.BrowserDpiNum.Size = new System.Drawing.Size(88, 20);
            this.BrowserDpiNum.TabIndex = 9;
            this.BrowserDpiNum.Value = new decimal(new int[] {
            96,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Browser DPI:";
            // 
            // dpiNum
            // 
            this.dpiNum.Location = new System.Drawing.Point(125, 64);
            this.dpiNum.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.dpiNum.Name = "dpiNum";
            this.dpiNum.Size = new System.Drawing.Size(88, 20);
            this.dpiNum.TabIndex = 11;
            this.dpiNum.Value = new decimal(new int[] {
            150,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(59, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Image DPI:";
            // 
            // countNum
            // 
            this.countNum.Location = new System.Drawing.Point(124, 94);
            this.countNum.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.countNum.Name = "countNum";
            this.countNum.Size = new System.Drawing.Size(88, 20);
            this.countNum.TabIndex = 13;
            this.countNum.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Duplicate Count:";
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPdf);
            this.tabControl.Controls.Add(this.tabHtml);
            this.tabControl.Location = new System.Drawing.Point(234, 42);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(494, 198);
            this.tabControl.TabIndex = 14;
            // 
            // tabPdf
            // 
            this.tabPdf.Controls.Add(this.label8);
            this.tabPdf.Controls.Add(this.compression);
            this.tabPdf.Controls.Add(this.label7);
            this.tabPdf.Controls.Add(this.DocumentOrientationBox);
            this.tabPdf.Controls.Add(this.label6);
            this.tabPdf.Controls.Add(this.DocumentSizeBox);
            this.tabPdf.Controls.Add(this.groupBox1);
            this.tabPdf.Controls.Add(this.generatePdfBtn);
            this.tabPdf.Controls.Add(this.label2);
            this.tabPdf.Controls.Add(this.pdfDpiNum);
            this.tabPdf.Location = new System.Drawing.Point(4, 22);
            this.tabPdf.Name = "tabPdf";
            this.tabPdf.Padding = new System.Windows.Forms.Padding(3);
            this.tabPdf.Size = new System.Drawing.Size(486, 172);
            this.tabPdf.TabIndex = 1;
            this.tabPdf.Text = "PDF";
            this.tabPdf.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 34);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(99, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Compression Level:";
            // 
            // compression
            // 
            this.compression.Location = new System.Drawing.Point(112, 30);
            this.compression.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.compression.Name = "compression";
            this.compression.Size = new System.Drawing.Size(88, 20);
            this.compression.TabIndex = 19;
            this.compression.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 98);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Orientation:";
            // 
            // DocumentOrientationBox
            // 
            this.DocumentOrientationBox.DisplayMember = "A4";
            this.DocumentOrientationBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.DocumentOrientationBox.Items.AddRange(new object[] {
            "Portrait",
            "Landscape"});
            this.DocumentOrientationBox.Location = new System.Drawing.Point(73, 95);
            this.DocumentOrientationBox.Name = "DocumentOrientationBox";
            this.DocumentOrientationBox.Size = new System.Drawing.Size(189, 21);
            this.DocumentOrientationBox.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(37, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Size:";
            // 
            // DocumentSizeBox
            // 
            this.DocumentSizeBox.DisplayMember = "A4";
            this.DocumentSizeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.DocumentSizeBox.Items.AddRange(new object[] {
            "_11X17",
            "A0",
            "A1",
            "A2",
            "A3",
            "A4",
            "A5",
            "A6",
            "A7",
            "A8",
            "A9",
            "A10",
            "ARCH_A",
            "ARCH_B",
            "ARCH_C",
            "ARCH_D",
            "ARCH_E",
            "B0",
            "B1",
            "B2",
            "B3",
            "B4",
            "B5",
            "B6",
            "B7",
            "B8",
            "B9",
            "B10",
            "CROWN_OCTAVO",
            "CROWN_QUARTO",
            "DEMY_OCTAVO",
            "DEMY_QUARTO",
            "EXECUTIVE",
            "FLSA",
            "FLSE",
            "HALFLETTER",
            "ID_1",
            "ID_2",
            "ID_3",
            "LARGE_CROWN_OCTAVO",
            "LARGE_CROWN_QUARTO",
            "LEDGER",
            "LEGAL",
            "LETTER",
            "NOTE",
            "PENGUIN_LARGE_PAPERBACK",
            "PENGUIN_SMALL_PAPERBACK",
            "POSTCARD",
            "ROYAL_OCTAVO",
            "ROYAL_QUARTO",
            "SMALL_PAPERBACK",
            "TABLOID"});
            this.DocumentSizeBox.Location = new System.Drawing.Point(73, 68);
            this.DocumentSizeBox.Name = "DocumentSizeBox";
            this.DocumentSizeBox.Size = new System.Drawing.Size(189, 21);
            this.DocumentSizeBox.TabIndex = 14;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.docMarginRightNum);
            this.groupBox1.Controls.Add(this.docMarginLeftNum);
            this.groupBox1.Controls.Add(this.docMarginDownNum);
            this.groupBox1.Controls.Add(this.docMarginUpNum);
            this.groupBox1.Location = new System.Drawing.Point(268, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(212, 110);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Document Margins (in mm)";
            // 
            // docMarginRightNum
            // 
            this.docMarginRightNum.DecimalPlaces = 1;
            this.docMarginRightNum.Location = new System.Drawing.Point(140, 52);
            this.docMarginRightNum.Name = "docMarginRightNum";
            this.docMarginRightNum.Size = new System.Drawing.Size(61, 20);
            this.docMarginRightNum.TabIndex = 4;
            this.docMarginRightNum.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // docMarginLeftNum
            // 
            this.docMarginLeftNum.DecimalPlaces = 1;
            this.docMarginLeftNum.Location = new System.Drawing.Point(6, 52);
            this.docMarginLeftNum.Name = "docMarginLeftNum";
            this.docMarginLeftNum.Size = new System.Drawing.Size(61, 20);
            this.docMarginLeftNum.TabIndex = 2;
            this.docMarginLeftNum.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // docMarginDownNum
            // 
            this.docMarginDownNum.DecimalPlaces = 1;
            this.docMarginDownNum.Location = new System.Drawing.Point(73, 84);
            this.docMarginDownNum.Name = "docMarginDownNum";
            this.docMarginDownNum.Size = new System.Drawing.Size(61, 20);
            this.docMarginDownNum.TabIndex = 1;
            this.docMarginDownNum.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // docMarginUpNum
            // 
            this.docMarginUpNum.DecimalPlaces = 1;
            this.docMarginUpNum.Location = new System.Drawing.Point(73, 19);
            this.docMarginUpNum.Name = "docMarginUpNum";
            this.docMarginUpNum.Size = new System.Drawing.Size(61, 20);
            this.docMarginUpNum.TabIndex = 0;
            this.docMarginUpNum.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // generatePdfBtn
            // 
            this.generatePdfBtn.Location = new System.Drawing.Point(6, 122);
            this.generatePdfBtn.Name = "generatePdfBtn";
            this.generatePdfBtn.Size = new System.Drawing.Size(474, 44);
            this.generatePdfBtn.TabIndex = 10;
            this.generatePdfBtn.Text = "Generate PDF File";
            this.generatePdfBtn.UseVisualStyleBackColor = true;
            this.generatePdfBtn.Click += new System.EventHandler(this.generatePdfBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "PDF DPI:";
            // 
            // pdfDpiNum
            // 
            this.pdfDpiNum.Location = new System.Drawing.Point(112, 4);
            this.pdfDpiNum.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.pdfDpiNum.Name = "pdfDpiNum";
            this.pdfDpiNum.Size = new System.Drawing.Size(88, 20);
            this.pdfDpiNum.TabIndex = 12;
            this.pdfDpiNum.Value = new decimal(new int[] {
            72,
            0,
            0,
            0});
            // 
            // tabHtml
            // 
            this.tabHtml.Controls.Add(this.GenerateHtmlButton);
            this.tabHtml.Controls.Add(this.label3);
            this.tabHtml.Controls.Add(this.BrowserDpiNum);
            this.tabHtml.Location = new System.Drawing.Point(4, 22);
            this.tabHtml.Name = "tabHtml";
            this.tabHtml.Padding = new System.Windows.Forms.Padding(3);
            this.tabHtml.Size = new System.Drawing.Size(486, 172);
            this.tabHtml.TabIndex = 0;
            this.tabHtml.Text = "HTML";
            this.tabHtml.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.picMarginRightNum);
            this.groupBox2.Controls.Add(this.picMarginLeftNum);
            this.groupBox2.Controls.Add(this.picMarginDownNum);
            this.groupBox2.Controls.Add(this.picMarginUpNum);
            this.groupBox2.Location = new System.Drawing.Point(12, 126);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(212, 110);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Picture Margins (in mm)";
            // 
            // picMarginRightNum
            // 
            this.picMarginRightNum.DecimalPlaces = 1;
            this.picMarginRightNum.Location = new System.Drawing.Point(140, 52);
            this.picMarginRightNum.Name = "picMarginRightNum";
            this.picMarginRightNum.Size = new System.Drawing.Size(61, 20);
            this.picMarginRightNum.TabIndex = 4;
            // 
            // picMarginLeftNum
            // 
            this.picMarginLeftNum.DecimalPlaces = 1;
            this.picMarginLeftNum.Location = new System.Drawing.Point(6, 52);
            this.picMarginLeftNum.Name = "picMarginLeftNum";
            this.picMarginLeftNum.Size = new System.Drawing.Size(61, 20);
            this.picMarginLeftNum.TabIndex = 2;
            // 
            // picMarginDownNum
            // 
            this.picMarginDownNum.DecimalPlaces = 1;
            this.picMarginDownNum.Location = new System.Drawing.Point(73, 84);
            this.picMarginDownNum.Name = "picMarginDownNum";
            this.picMarginDownNum.Size = new System.Drawing.Size(61, 20);
            this.picMarginDownNum.TabIndex = 1;
            // 
            // picMarginUpNum
            // 
            this.picMarginUpNum.DecimalPlaces = 1;
            this.picMarginUpNum.Location = new System.Drawing.Point(73, 19);
            this.picMarginUpNum.Name = "picMarginUpNum";
            this.picMarginUpNum.Size = new System.Drawing.Size(61, 20);
            this.picMarginUpNum.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(740, 262);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.countNum);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dpiNum);
            this.Controls.Add(this.output);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pathTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.browseButton);
            this.Name = "Form1";
            this.Text = "Image Sheet Creator";
            ((System.ComponentModel.ISupportInitialize)(this.BrowserDpiNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dpiNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.countNum)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.tabPdf.ResumeLayout(false);
            this.tabPdf.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.compression)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.docMarginRightNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.docMarginLeftNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.docMarginDownNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.docMarginUpNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdfDpiNum)).EndInit();
            this.tabHtml.ResumeLayout(false);
            this.tabHtml.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picMarginRightNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMarginLeftNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMarginDownNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMarginUpNum)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button GenerateHtmlButton;
        private System.Windows.Forms.FolderBrowserDialog folderBrowser;
        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox pathTextBox;
        private System.Windows.Forms.Label output;
        private System.Windows.Forms.NumericUpDown BrowserDpiNum;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown dpiNum;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown countNum;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabHtml;
        private System.Windows.Forms.TabPage tabPdf;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown docMarginLeftNum;
        private System.Windows.Forms.NumericUpDown docMarginDownNum;
        private System.Windows.Forms.NumericUpDown docMarginUpNum;
        private System.Windows.Forms.Button generatePdfBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown pdfDpiNum;
        private System.Windows.Forms.NumericUpDown docMarginRightNum;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown picMarginRightNum;
        private System.Windows.Forms.NumericUpDown picMarginLeftNum;
        private System.Windows.Forms.NumericUpDown picMarginDownNum;
        private System.Windows.Forms.NumericUpDown picMarginUpNum;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox DocumentOrientationBox;
        private System.Windows.Forms.ComboBox DocumentSizeBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown compression;
    }
}

