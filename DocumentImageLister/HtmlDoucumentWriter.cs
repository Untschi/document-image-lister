﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentImageLister
{
    public class HtmlDoucumentWriter : DocumentWriter
    {
        public int DPI;
        protected override int DocumentDPI { get { return DPI; } }

        public Rect PicMargins;

        StringBuilder sb = new StringBuilder();
        string path;

        public override void CreateDocument(string path)
        {
            this.path = path;

            sb.Clear();
            sb.AppendLine("<html>");
            sb.AppendLine("\t<body>");
        }

        public override void CloseDocument()
        {
            sb.AppendLine("\t</body>");
            sb.AppendLine("</html>");



            string file = Path.Combine(path, "images.html");
            if (File.Exists(file))
                File.Delete(file);

            using (StreamWriter writer = File.CreateText(file))
            {
                writer.Write(sb.ToString());
                writer.Close();
            }

            System.Diagnostics.Process.Start(file);
        }

        protected override void AddImage(string imagePath, int width, int height)
        {
            sb.Append(string.Format("<img src=\"{0}\"  width=\"{1}\" height=\"{2}\" style=\"margin: {3}px {4}px {5}px {6}px;\"></img>",
                System.Web.HttpUtility.HtmlEncode(Path.GetFileName(imagePath)),
                width, height, 
                PicMargins.Top, PicMargins.Width, PicMargins.Height, PicMargins.Left));
        }

    }
}
