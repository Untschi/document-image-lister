﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentImageLister
{
    public abstract class DocumentWriter
    {
        protected abstract int DocumentDPI { get; }

        public abstract void CreateDocument(string path);
        public abstract void CloseDocument();
        protected abstract void AddImage(string imagePath, int width, int height);

        public void AddImage(string imagePath, int width, int height, int imageDPI)
        {
            Point size = Resize(width, height, imageDPI);

            AddImage(imagePath, size.X, size.Y);
        }

        protected Point Resize(int width, int height, int inputDPI)
        {
            return new Point(
                (int)((float)width * DocumentDPI / inputDPI),
                (int)((float)height * DocumentDPI / inputDPI) );
        }
    }
}
